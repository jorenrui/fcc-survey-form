# FCC Survey Form

![freeCodeCamp Survey Form](assets/img/survey-form.png)

This is a Project Requirement for the [freeCodeCamp Responsive Web Design Certification](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-survey-form). This is built as to practice handling form elements. See the website: [https://jorenrui.gitlab.io/fcc-survey-form](https://jorenrui.gitlab.io/fcc-survey-form)

## Development Process

### Test Suite

> [testable-projects-fcc](https://github.com/freeCodeCamp/testable-projects-fcc)
>
> A CDN loaded test-suite for testing the freecodecamp.com Certification waypoint projects.

**Technology Stack**

- [x] You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

**Content**

- [x] **User Story #1**: I can see a title with id="title" in H1 sized text.
- [x] **User Story #2:** I can see a short explanation with `id="description"` in P sized text.
- [x] **User Story #3:** I can see a `form` with `id="survey-form"`.
- [x] **User Story #4:** Inside the form element, I am required to enter my name in a field with `id="name"`.
- [x] **User Story #5:** Inside the form element, I am required to enter an email in a field with `id="email"`.
- [x] **User Story #6:** If I enter an email that is not formatted correctly, I will see an HTML5 validation error.
- [x] **User Story #7:** Inside the form, I can enter a number in a field with `id="number"`.
- [x] **User Story #8:** If I enter non-numbers in the number input, I will see an HTML5 validation error.
- [x] **User Story #9:** If I enter numbers outside the range of the number input, which are defined by the `min` and `max` attributes, I will see an HTML5 validation error.
- [x] **User Story #10:** For the name, email, and number input fields inside the form I can see corresponding labels that describe the purpose of each field with the following ids: `id="name-label"`, `id="email-label"`, and `id="number-label"`.
- [x] **User Story #11:** For the name, email, and number input fields, I can see placeholder text that gives me a description or instructions for each field.
- [x] **User Story #12:** Inside the form element, I can select an option from a dropdown that has a corresponding `id="dropdown"`.
- [x] **User Story #13:** Inside the form element, I can select a field from one or more groups of radio buttons. Each group should be grouped using the `name` attribute.
- [x] **User Story #14:** Inside the form element, I can select several fields from a series of checkboxes, each of which must have a `value` attribute.
- [x] **User Story #15:** Inside the form element, I am presented with a `textarea` at the end for additional comments.
- [x] **User Story #16:** Inside the form element, I am presented with a button with `id="submit"` to submit all my inputs.

### Design

- Traditional Pen and Paper for creating a Layout (Wireframe).

- [Adobe Experience Design](https://www.adobe.com/sea/products/xd.html) for design and prototyping. [See design](designs/survey-form.xd).

![FCC Survey Form Design using Adobe XD](assets/img/adobe-xd.png)

### Development

> Note: Added some improvements to the design in the development phase. So slight differences between the design and the final product can be seen.

**History:**

1. [CSS Grid Layout](assets/img/app/app1.png)
2. [Created Header Section](assets/img/app/app2.png)
3. [Worked on the Form Section](assets/img/app/app3.png)
4. [Added Animations and input elements](assets/img/app/app4.png)
5. [Added Radio Buttons](assets/img/app/app5.png)
6. [Added Textbox and Textarea](assets/img/app/app6.png)
7. [Added buttons](assets/img/app/app7-1.png) and [Added a Form Submit Page](assets/img/app/app7-2.png)
8. Mobile Responsiveness for [Header Section](assets/img/app/app8-1.png), [Form Section](assets/img/app/app8-2.png), and [Form Submit Page](assets/img/app/app8-3.png)

## References that helped me

- [Removing Circle Button in Radio Box](https://jsfiddle.net/petrabarus/pPgS7/)
- [Styling Radio Buttons](https://teamgaslight.com/blog/til-styling-radio-buttons)
- [Can I hide the HTML5 number input’s spin box?](https://stackoverflow.com/questions/3790935/can-i-hide-the-html5-number-input-s-spin-box)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
